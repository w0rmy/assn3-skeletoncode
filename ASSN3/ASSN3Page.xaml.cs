﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebURL
	{	
		// Applies to Id.
		[SQLite.PrimaryKey, SQLite.AutoIncrement]
		public int Id { get; set; }

		public string Url { get; set; }
		public string Picture { get; set; }
		public string Title { get; set; }
	}

	public partial class ASSN3Page : ContentPage
	{
		// SQL Constructor
		private SQLiteAsyncConnection _connection;
		// List called urls of WebURL type
		private List<WebURL> urls;

		public ASSN3Page()
		{
			// Constructor connecting to SQL database
			_connection = DependencyService.Get<ISQLiteDb>().GetConnection();
			// Create default DB if DB empty
			DefaultDB();

			InitializeComponent();
		}

		// Populate a default database if database is empty.
		async void DefaultDB()
		{
			// Creates DB table using the WebUrl class. If table exists it doesnt do anything.
			await _connection.CreateTableAsync<WebURL>();

			// Check if table is empty, if so populates it with default entries.
			urls = await _connection.Table<WebURL>().ToListAsync();
			if (urls.Count == 0)
			{
				var google = new WebURL { Title = "Google", Picture = "", Url = "https://www.google.co.nz" };
				await _connection.InsertAsync(google);
				var youtube = new WebURL { Title = "YouTube", Picture = "", Url = "https://www.youtube.com" };
				await _connection.InsertAsync(youtube);
				var facebook = new WebURL { Title = "Facebook", Picture = "", Url = "https://www.facebook.com" };
				await _connection.InsertAsync(facebook);

				// Reload the picker after populating database
				ReloadPicker();
			}
		}

		// Reload picker method
		async void ReloadPicker()
		{
			// Collects data
			urls = await _connection.Table<WebURL>().ToListAsync();

			if (ShowURLs.Items.Count != 0)
			{
				ShowURLs.Items.Clear();
			}

			foreach (var x in urls)
			{
				ShowURLs.Items.Add($"{x.Title}");
			}
		}

		// Method to call everytime a page appears
		protected override void OnAppearing()
		{
			ReloadPicker();

			base.OnAppearing();
		}

		async void OpenWebView(object sender, System.EventArgs e)
		{
			if (ShowURLs.SelectedIndex == -1)
			{
				await DisplayAlert("OOOPS", "Please select a URL", "OK");
			}
			else
			{
				var passOnUrl = urls[ShowURLs.SelectedIndex].Url;
				await Navigation.PushModalAsync(new WebViewShow(passOnUrl));
			}
		}

		async void AddURLAction(object sender, System.EventArgs e)
		{
			await Navigation.PushModalAsync(new AddURLPage());
		}

		async void UpdateURLAction(object sender, System.EventArgs e)
		{
			if (ShowURLs.SelectedIndex == -1)
			{
				await DisplayAlert("OOOPS", "Please select a URL", "OK");
			}
			else
			{
				urls = await _connection.Table<WebURL>().ToListAsync();

				var selectedItems = ShowURLs.SelectedIndex;
				var selectedClass = urls[ShowURLs.SelectedIndex];

				await Navigation.PushModalAsync(new EditURLPage(selectedClass, selectedItems));
			}
		}

		async void RemoveURL(object sender, System.EventArgs e)
		{
			if (ShowURLs.SelectedIndex == -1)
			{
				await DisplayAlert("OOOPS", "Please select a URL", "OK");
			}
			else
			{
				urls = await _connection.Table<WebURL>().ToListAsync();
				var selectedItems = urls[ShowURLs.SelectedIndex];
				await _connection.DeleteAsync(selectedItems);
				await DisplayAlert("YAY!!", "URL removed", "OK");
			}

			ReloadPicker();
		}
	}
}