﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class EditURLPage : ContentPage
	{
		private SQLiteAsyncConnection _connection;
		private ObservableCollection<WebURL> _url;
		private int SelectedNumber;

		public EditURLPage(WebURL selectedClass, int selection)
		{
			InitializeComponent();

			_connection = DependencyService.Get<ISQLiteDb>().GetConnection();

			BindingContext = selectedClass;
			SelectedNumber = selection;
		}

		async void EditURL(object sender, EventArgs e)
		{
			if (TitleNew.Text == null)
			{
				await DisplayAlert("OOOPS", "Please enter a name", "OK");
			}
			if (URLNew.Text == null)
			{
				await DisplayAlert("OOOPS", "Please enter a URL", "OK");
			}
			else
			{
				var url = await _connection.Table<WebURL>().ToListAsync();
				_url = new ObservableCollection<WebURL>(url);

				var urlBlock = _url[SelectedNumber];

				urlBlock.Title = TitleNew.Text;
				urlBlock.Url = URLNew.Text;
				urlBlock.Picture = ImageNew.Text;

				await _connection.UpdateAsync(urlBlock);
				await DisplayAlert("YAY!!!", "URL updated", "OK");
				await Navigation.PopModalAsync();
			}
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}