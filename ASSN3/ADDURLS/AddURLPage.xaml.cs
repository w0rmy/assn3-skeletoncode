﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class AddURLPage : ContentPage
	{
		private SQLiteAsyncConnection _connection;

		public AddURLPage()
		{
			InitializeComponent();

			// Constructor connecting to SQL database
			_connection = DependencyService.Get<ISQLiteDb>().GetConnection();
		}

		async void AddURL(object sender, EventArgs e)
		{
			if (TitleNew.Text == null)
			{
				await DisplayAlert("OOOPS", "Please enter a name", "OK");
			}
			if (URLNew.Text == null)
			{
				await DisplayAlert("OOOPS", "Please enter a URL", "OK");
			}
			else
			{
				var url = new WebURL { Title = TitleNew.Text, Picture = ImageNew.Text, Url = URLNew.Text };
				await _connection.InsertAsync(url);

				await DisplayAlert("YAY!!!", "URL added", "OK");

				TitleNew.Text = "";
				ImageNew.Text = "";
				URLNew.Text = "";
			}
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}

	}
}
